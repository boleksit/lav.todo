﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Steelkiwi.Com.Library;

namespace lav.todo
{


    class RestConnector
    {
        private static string baseUrl = "http://restapi-test.eu-central-1.elasticbeanstalk.com/";

        public static async Task<List<ActionItem>> RefreshDataAsync(DotsLoaderView dotsLoaderView)
        {
            var items = new List<ActionItem>();
            dotsLoaderView.Show();
            string requestUrl = baseUrl + "api/action/";
            await Task.Run(async () =>
            {
                using (HttpClient client = new HttpClient())
                {


                    HttpResponseMessage response = await client.GetAsync(requestUrl);
                    if (response.IsSuccessStatusCode)
                    {
                        var json = await response.Content.ReadAsStringAsync();
                        items = Deserialize(items, json);
                    }
                }
               
            });
            dotsLoaderView.Hide();
            return items;
        }
        public static async Task<ActionItem> GetDataAsync(long Id)
        {
            List<ActionItem> items = new List<ActionItem>();
            string requestUrl = baseUrl + "api/action/"+Id.ToString();
            await Task.Run(async () =>
            {
                using (HttpClient client = new HttpClient())
                {


                    HttpResponseMessage response = await client.GetAsync(requestUrl);
                    if (response.IsSuccessStatusCode)
                    {
                        var json = await response.Content.ReadAsStringAsync();
                        items = Deserialize(items, json);
                    }
                }
                
            });
            return items[0];

        }

        private static List<ActionItem> Deserialize(List<ActionItem> items, string json)
        {

            foreach (ActionItem item in JsonConvert.DeserializeObject<IEnumerable<ActionItem>>(json) as List<ActionItem>)
            {
                if (!item.isDone)
                {
                    items.Add(item);
                }
            }

            return items;

        }

        public static async Task PutDataAsync(ActionItem action)
        {
            string requestUrl = baseUrl + "api/action/" + action.ID;
            using (HttpClient client = new HttpClient())
            {
                string sData = JsonConvert.SerializeObject(action);
                HttpContent content = new StringContent(sData, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PutAsync(requestUrl, content);

            }
        }
        public static async Task<int> AddDataAsync(ActionItem item)
        {
            string requestUrl = baseUrl + "api/action/";

            using (HttpClient client = new HttpClient())
            {
                string sData = JsonConvert.SerializeObject(item);
                HttpContent content = new StringContent(sData, Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync(requestUrl, content);

                int lastIndex = response.Headers.Location.ToString().LastIndexOf('/');

                return Int32.Parse(response.Headers.Location.ToString().Substring(lastIndex + 1));


            }

        }
    }
}