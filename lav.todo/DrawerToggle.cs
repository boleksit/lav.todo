﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SupportDrawerToggle = Android.Support.V7.App.ActionBarDrawerToggle;
using Android.Support.V7.App;
using Android.Support.V4.Widget;

namespace lav.todo
{
    class DrawerToggle : SupportDrawerToggle
    {

        private MainActivity mHostActivity;
        private int mOpenedResource;
        private int mClosedResource;

        public DrawerToggle(MainActivity host, DrawerLayout drawerLayout, int openedResource, int closeResource)
            : base(host, drawerLayout, openedResource, closeResource)
        {
            mHostActivity = host;
            mOpenedResource = openedResource;
            mClosedResource = closeResource;

        }

        public override void OnDrawerOpened(View drawerView)
        {
            base.OnDrawerOpened(drawerView);
            mHostActivity.SupportActionBar.SetTitle(mOpenedResource);
        }

        public override void OnDrawerClosed(View drawerView)
        {
            base.OnDrawerClosed(drawerView);
            mHostActivity.SupportActionBar.SetTitle(mClosedResource);

        }

        public override void OnDrawerSlide(View drawerView, float slideOffset)
        {
            base.OnDrawerSlide(drawerView, slideOffset);
        }
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.userLogout:
                    //SignOut();
                    return true;
                case Resource.Id.action_refresh:
                    mHostActivity.RefreshData();
                    return true;
                default:
                    return base.OnOptionsItemSelected(item);
            }
        }
       
    }
}