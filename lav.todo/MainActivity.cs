﻿using System;
using Android.App;
using Android.Content;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Collections.Generic;
using System.Threading.Tasks;
using Android.Support.V7.App;
using Steelkiwi.Com.Library;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V4.Widget;
using Android.Gms.Common.Apis;
using Android.Gms.Common;
using Android.Gms.Auth.Api.SignIn;
using Android.Gms.Auth.Api;
using Android.Support.Design.Widget;
using DE.Hdodenhof.Circleimageview;
using Android.Graphics;
using System.Net;



namespace lav.todo
{
    [Activity(Label = "lav.todo", MainLauncher = true, Icon = "@mipmap/icon", Theme = "@style/MyTheme")]
    public class MainActivity : AppCompatActivity, GoogleApiClient.IOnConnectionFailedListener, View.IOnClickListener, NavigationView.IOnNavigationItemSelectedListener
    {
        private List<ActionItem> actionItems = new List<ActionItem>();
        private ListView listView;
        private ListAdapter adapter;
        private DotsLoaderView dotsLoaderView;
        private TableRow tableRow;
        private ImageButton btAdd;
        private EditText txAdd;
        private SupportToolbar mTooblbar;
        private DrawerToggle mDrawerToggle;
        private DrawerLayout mDrawerLayout;

        private TextView userName;
        private GoogleApiClient mGoogleApiClient;
        //private Menu menu;
        private GoogleSignInAccount mGoogleUser;
        private View navHeader;
        private NavigationView navigationView;
        private CircleImageView imgProfile;
        const int RC_SIGN_IN = 9001;
        public bool isSignedIn = false;
        protected override void OnCreate(Bundle savedInstanceState)
        {

            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Main);

            //Inicjowanie kontrolek

            mTooblbar = FindViewById<SupportToolbar>(Resource.Id.toolbar);
            tableRow = FindViewById<TableRow>(Resource.Id.myTableRow);
            btAdd = FindViewById<ImageButton>(Resource.Id.btAdd);
            txAdd = FindViewById<EditText>(Resource.Id.txAdd);
            dotsLoaderView = FindViewById<DotsLoaderView>(Resource.Id.dotsLoaderView);
            listView = FindViewById<ListView>(Resource.Id.myListView1);
            mDrawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            navigationView = (NavigationView)FindViewById(Resource.Id.left_drawer);
            navHeader = navigationView.GetHeaderView(0);
            userName = navHeader.FindViewById<TextView>(Resource.Id.userName);
            imgProfile = navHeader.FindViewById<CircleImageView>(Resource.Id.img_profile);

            //   Google Integration

            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DefaultSignIn)
                .RequestProfile()
                .RequestEmail()
                .Build();




            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .EnableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                    .AddApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .Build();


            //   End of Google Integration
            SetSupportActionBar(mTooblbar);
            mDrawerToggle = new DrawerToggle(this, mDrawerLayout, Resource.String.openDrawer, Resource.String.closeDrawer);
            mDrawerLayout.AddDrawerListener(mDrawerToggle);

            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(true);

            mDrawerToggle.SyncState();
            navigationView.SetNavigationItemSelectedListener(this);


            if (savedInstanceState != null)
            {
                if (savedInstanceState.GetString("DrawerState") == "Opened")
                {
                    SupportActionBar.SetTitle(Resource.String.openDrawer);

                }
                else
                {

                    SupportActionBar.SetTitle(Resource.String.closeDrawer);
                }
            }

            else
            {
                SupportActionBar.SetTitle(Resource.String.closeDrawer);
            }
            listView.Adapter = adapter = new ListAdapter(this, actionItems);

            btAdd.Click += async (sender, e) =>
            {
                if (!string.IsNullOrEmpty(txAdd.Text))
                {
                    ActionItem item = new ActionItem(txAdd.Text);
                    item.ID = await AddData(item);
                    txAdd.Text = "";
                }

            };


            RefreshData();


        }

        public void RefreshData()
        {
            adapter.RestetEdit();
            RestConnector.RefreshDataAsync(dotsLoaderView).ContinueWith(t =>
            {

                var result = t.Result;
                
                foreach (ActionItem item in result)
                {
                    if (!adapter.ItemExistById(item.ID))
                    {
                        item.edited = 1;
                        adapter.Add(item);
                    }
                    else
                    {
                        int pos = adapter.FindPositionById(item.ID);
                        item.edited = 1;
                        if (pos >= 0)
                            adapter.Edit(item, pos);
                    }
                }
                
                    adapter.Validate();
                    //TODO krzaczy na końcy metody
               
            });
        }

        private async Task<Bitmap> GetImageBitmapFromUrl(string url)
        {
            Bitmap imageBitmap = null;
            try
            {

                using (var webClient = new WebClient())
                {
                    var imageBytes = await webClient.DownloadDataTaskAsync(url);
                    if (imageBytes != null && imageBytes.Length > 0)
                    {
                        imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return imageBitmap;
        }

        //Google Login



        protected override void OnStart()
        {
            base.OnStart();

            var opr = Auth.GoogleSignInApi.SilentSignIn(mGoogleApiClient);

            if (opr.IsDone)
            {
                // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
                // and the GoogleSignInResult will be available instantly.

                var result = opr.Get() as GoogleSignInResult;
                HandleSignInResult(result);
            }
            else
            {
                // If the user has not previously signed in on this device or the sign-in has expired,
                // this asynchronous branch will attempt to sign in the user silently.  Cross-device
                // single sign-on will occur in this branch.

                //ShowProgressDialog();
                SignIn();
                //opr.SetResultCallback(new SignInResultCallback { Activity = this });
            }

        }
        protected override void OnStop()
        {
            base.OnStop();
            if (mGoogleApiClient.IsConnected)
            {
                mGoogleApiClient.Disconnect();
            }
        }

        protected override void OnResume()
        {
            base.OnResume();
            HideProgressDialog();
        }

        public void ShowProgressDialog()
        {
            dotsLoaderView.Show();
        }

        public void HideProgressDialog()
        {
            dotsLoaderView.Hide();
        }


        public void HandleSignInResult(GoogleSignInResult result)
        {

            if (result.IsSuccess)
            {
                // Signed in successfully, show authenticated UI.

                mGoogleUser = result.SignInAccount;
                //adapter.Add(new ActionItem(mGoogleUser.DisplayName));
                isSignedIn = true;
                UpdateUi();
                //LoadNavHeader();

            }
            else
            {
                // Signed out, show unauthenticated UI.
                isSignedIn = false;
                UpdateUi();

            }
        }

        void SignIn()
        {
            var signInIntent = Auth.GoogleSignInApi.GetSignInIntent(mGoogleApiClient);
            StartActivityForResult(signInIntent, RC_SIGN_IN);
        }

        void SignOut()
        {
            Auth.GoogleSignInApi.SignOut(mGoogleApiClient).SetResultCallback(new SignOutResultCallback { Activity = this });


        }

        void RevokeAccess()
        {
            Auth.GoogleSignInApi.RevokeAccess(mGoogleApiClient).SetResultCallback(new SignOutResultCallback { Activity = this });
        }

        public async void UpdateUi()
        {
            if (isSignedIn)
            {
                if (mGoogleUser.DisplayName != null)
                {
                    if (mGoogleUser.DisplayName == mGoogleUser.Email)
                        userName.Text = new String(mGoogleUser.DisplayName.ToCharArray(), 0, mGoogleUser.DisplayName.IndexOf('@'));
                    else
                        userName.Text = mGoogleUser.DisplayName;
                }
                else
                    userName.Text = "User";

                if (mGoogleUser.Email != null)
                    FindViewById<TextView>(Resource.Id.userMail).Text = mGoogleUser.Email;
                else
                    FindViewById<TextView>(Resource.Id.userMail).Text = "";
                if (mGoogleUser.PhotoUrl != null)
                {
                    var image = await GetImageBitmapFromUrl(mGoogleUser.PhotoUrl.ToString());

                    imgProfile.SetImageBitmap(image);
                }
                else
                    imgProfile.SetImageResource(Resource.Drawable.profile);

                navigationView.Menu.FindItem(Resource.Id.userLogin).SetVisible(false);
                navigationView.Menu.FindItem(Resource.Id.userLogout).SetVisible(true);

            }
            else
            {
                userName.Text = "User";
                FindViewById<TextView>(Resource.Id.userMail).Text = "";

                imgProfile.SetImageResource(Resource.Drawable.profile);

                navigationView.Menu.FindItem(Resource.Id.userLogin).SetVisible(true);
                navigationView.Menu.FindItem(Resource.Id.userLogout).SetVisible(false);



            }
        }
        //End Google login



        protected async Task<long> AddData(ActionItem item)
        {
            Task<int> task = RestConnector.AddDataAsync(item);
            item.ID = await task;
            adapter.Add(item);
            return item.ID;
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            switch (requestCode)
            {
                case RC_SIGN_IN:
                    {
                        var result = Auth.GoogleSignInApi.GetSignInResultFromIntent(data);
                        HandleSignInResult(result);

                    }
                    break;
                case 1:
                    {
                        if (resultCode == Result.Ok)
                        {
                            ActionItem item = new ActionItem(data.GetStringExtra("actionText"), Convert.ToBoolean(data.GetIntExtra("isDone", 0)), data.GetIntExtra("position", 0));
                            adapter.Edit(item, actionItems.FindIndex(x => x.ID == item.ID));
                        }
                    }
                    break;
            }

        }
        void ListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            adapter.BtEditClick(e.Position);
        }
        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.action_menu, menu);
            //if (isSignedIn)
            //{
            //    menu.SetGroupVisible(Resource.Id.menuLogin, false);
            //    menu.FindItem(Resource.Id.userLogin).SetEnabled(false);
            //    menu.SetGroupVisible(Resource.Id.menuLogout, true);
            //    menu.FindItem(Resource.Id.userLogout).SetEnabled(true);
            //}
            //else
            //{
            //    menu.SetGroupVisible(Resource.Id.menuLogin, true);
            //    menu.FindItem(Resource.Id.userLogin).SetEnabled(true);
            //    menu.SetGroupVisible(Resource.Id.menuLogout, false);
            //    menu.FindItem(Resource.Id.userLogout).SetEnabled(false);
            //}

            return true;
        }


        //Drawer controll
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.action_refresh:
                    //refresh
                    RefreshData();
                    return true;

                case Android.Resource.Id.Home:
                    //left drawer
                    mDrawerLayout.OpenDrawer(navigationView);
                    mDrawerToggle.OnOptionsItemSelected(item);
                    return true;
                case Android.Resource.Id.Toggle:
                    mDrawerLayout.CloseDrawer(navigationView);
                    return true;

                default:
                    return base.OnOptionsItemSelected(item);

            }


        }
        protected override void OnSaveInstanceState(Bundle outState)
        {

            if (mDrawerLayout.IsDrawerOpen((int)GravityFlags.Left))
            {
                outState.PutString("DrawerState", "Opened");

            }
            else
            {
                outState.PutString("DrawerState", "Closed");
            }
            base.OnSaveInstanceState(outState);
        }
        protected override void OnPostCreate(Bundle savedInstanceState)
        {
            base.OnPostCreate(savedInstanceState);
            mDrawerToggle.SyncState();
        }




        // End Drawer Controll


        // Interface method
        public void OnConnectionFailed(ConnectionResult result)
        {
            throw new NotImplementedException();
        }

        public void OnClick(View v)
        {
            switch (v.Id)
            {
                case Resource.Id.action_refresh:
                    RefreshData();
                    break;
                    //case Resource.Id.sign_out_button:
                    //    SignOut();
                    //    break;
                    //case Resource.Id.disconnect_button:
                    //    RevokeAccess();
                    //    break;
                    // }
            }
        }
        public bool OnNavigationItemSelected(IMenuItem menuItem)
        {
            switch (menuItem.ItemId)
            {
                case Resource.Id.userLogout:
                    SignOut();
                    isSignedIn = false;
                    return true;
                case Resource.Id.userLogin:
                    SignIn();
                    isSignedIn = true;
                    return true;

                case Resource.Id.action_refresh:
                    RefreshData();
                    return true;
                default:
                    return false;
            }
        }

        //End Interface Method
    }
}
