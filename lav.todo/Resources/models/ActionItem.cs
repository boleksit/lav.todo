﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace lav.todo
{
    public class ActionItem
    {
        public long ID { get; set; }
        public bool isDone { get; set; }
        public string actionText { get; set; }
        public int listNumber { get; set; }
        public int owner { get; set; }
        public int edited { get; set; } = 0;


        public ActionItem (string IActionText)
        {
            actionText = IActionText;
            isDone = false;
        }
        public ActionItem(string IActionText, bool IIsDone)
        {
            actionText = IActionText;
            isDone = IIsDone;
        }
        public ActionItem()
        {

        }

        public ActionItem(string IActionText, bool IIsDone, int IID)
        {
            actionText = IActionText;
            isDone = IIsDone;
            ID = IID;
        }
        public void Done ()
        {
            isDone = true;

        }

    }
}