﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;

namespace lav.todo
{
    [Activity(Label = "EditTask", Theme = "@style/MyTheme")]
    public class EditTask : AppCompatActivity
    {

        
        private ActionItem item;


        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.EditTask);
            
            SetSupportActionBar(FindViewById<SupportToolbar>(Resource.Id.edittoolbar));
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);

           

            item = new ActionItem(Intent.GetStringExtra("actionText"), Intent.GetBooleanExtra("isDone", false), Intent.GetIntExtra("position", 0));

            FindViewById<EditText>(Resource.Id.txTask).Text = item.actionText;

           

        }

        private async void EditUpdate()
        {
            Intent returnIntent = new Intent();
            item.actionText = FindViewById<EditText>(Resource.Id.txTask).Text;
            returnIntent.PutExtra("actionText", item.actionText);
            returnIntent.PutExtra("isDone", item.isDone);
            returnIntent.PutExtra("position", item.ID);

            await RestConnector.PutDataAsync(item);
            SetResult(Result.Ok, returnIntent);
            Finish();
        }

       
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    SetResult(Result.Canceled);
                    Finish();
                    return true; ;
                case Resource.Id.applyEdit:
                    EditUpdate();
                    return true; 

                default:
                    return base.OnOptionsItemSelected(item);
            }
        }
        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            
            
                MenuInflater inflater = MenuInflater;

                inflater.Inflate(Resource.Menu.edit_bar_menu, menu);
                return base.OnCreateOptionsMenu(menu);
            
        }
    }
}