﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using static Android.Widget.AdapterView;
using static Android.Resource;
using System.Threading.Tasks;

namespace lav.todo
{
    public class ListAdapter : BaseAdapter<ActionItem>
    {
        List<ActionItem> items;
        Activity context;
        ListView listView;
        public ListAdapter(Activity context, List<ActionItem> items)
            : base()
        {
            this.context = context;
            this.items = items;
            listView = context.FindViewById<ListView>(Resource.Id.myListView1);

        }
        public override long GetItemId(int position)
        {
            return position;
        }
        public override ActionItem this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }




        public void BtEditClick(int position)
        {
            var intent = new Intent(context, typeof(EditTask));
            intent.PutExtra("actionText", items[position].actionText);
            intent.PutExtra("isDone", items[position].isDone);
            intent.PutExtra("position", items[position].ID);

            context.StartActivityForResult(intent, 1);

        }



        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = convertView;

            var item = items[position];
            if (view == null)
            {
                view = context.LayoutInflater.Inflate(Resource.Layout.ListItem, parent, false);
                ImageButton btIsDone = view.FindViewById<ImageButton>(Resource.Id.btIsDone);
                //ImageButton btEdit = view.FindViewById<ImageButton>(Resource.Id.btEdit);

                btIsDone.Focusable = false;
                btIsDone.FocusableInTouchMode = false;
                btIsDone.Clickable = true;
               // btEdit.Focusable = false;
               // btEdit.FocusableInTouchMode = false;
               // btEdit.Clickable = true;
                btIsDone.SetTag(Resource.Id.btIsDone, position);
               // btEdit.SetTag(Resource.Id.btEdit, position);

                view.Click += (sender, e) =>
                {
                    BtEditClick(position);
                };
                btIsDone.Click += (object sender, EventArgs e) =>
                {

                    int delPos = (int)(((ImageButton)sender).GetTag(Resource.Id.btIsDone));
                    ActionItem itemEdit = items[delPos];
                    itemEdit.isDone = true;
                    Edit(itemEdit, delPos);
                    PutIsDone(itemEdit);
                    items.RemoveAt(delPos);
                    NotifyDataSetChanged();
                };
                //btEdit.Click += (object sender, EventArgs e) =>
                //{
                //    int editPos = (int)(((ImageButton)sender).GetTag(Resource.Id.btEdit));

                //    BtEditClick(editPos);
                //    NotifyDataSetChanged();
                //};


            }
            TextView textView = view.FindViewById<TextView>(Resource.Id.txLabel);


            textView.Text = items[position].actionText;


            return view;
        }


        public void Edit(ActionItem item, int position)
        {
            context.RunOnUiThread(() =>
            {
                items[position] = item;
                NotifyDataSetChanged();
            });
        }
        public void Add(ActionItem item)
        {
            context.RunOnUiThread(() =>
            {
                items.Add(item);
                NotifyDataSetChanged();
            });
        }
        public void Remove(ActionItem item)
        {


            context.RunOnUiThread(() =>
            {
                items.Remove(item);

                NotifyDataSetChanged();
            });

        }
        public void Remove(int pos)
        {


            context.RunOnUiThread(() =>
            {
                items.RemoveAt(pos);

                NotifyDataSetChanged();
            });

        }
        protected async void PutIsDone(ActionItem item)
        {
            await RestConnector.PutDataAsync(item);
        }

        public bool ItemExistById(long id)
        {
            for (int i = items.Count - 1; i >= 0; i--)
            {
                if (items[i].ID == id)
                    return true;
            }
            return false;
        }
        public int FindPositionById(long id)
        {
            for (int i = items.Count - 1; i >= 0; i--)
            {
                if (items[i].ID == id)
                    return i;

            }
            return -1;
        }
        public void Validate()
        {
            foreach (ActionItem item in items)
            {
                if (item.edited==0)
                    Remove(item);
            }
        }
        public void RestetEdit ()
        {
            foreach (ActionItem item in items)
            {
                item.edited = 0;
            }
        }
        public async void Update(List<int> visited)
        {
            //for (int i=items.Count-1;i>=0;i--)
            //{
            //    bool check = false;
            //    foreach (int vis in visited)
            //    {
            //        if (i == vis)
            //        {
            //            check = true;
            //            break;
            //        }
            //        else
            //            check = false;
            //    }
            //    if (!check)
            //    {
            //        ActionItem item = await Task.Run( ()  => RestConnector.GetDataAsync(items[i].ID));
                    
            //            Edit(item, i);
                   
            //    }
            //}
        }
    }

}

