﻿using System;
using System.Collections.Generic;


namespace lav.todo.Resources.models
{
    class ListItem
    {
        public enum ListTypes { ToDo, Shopping };

        public int ListId { get; set; }
        public int ListOwner { get; set; }
        public List<UserItem> ListUsers { get; set; }
        
        public ListTypes ListType { get; set; }

        public DateTime LastSync { get; set; }
    }
}